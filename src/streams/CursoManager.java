package streams;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class CursoManager {

    private static List<Curso> cursos;

    static {
        cursos = new ArrayList<>();
        cursos.add(new Curso("Cursos profesional de Java", 6.5f, 50, 200));
        cursos.add(new Curso("Cursos profesional de Python", 8.5f, 60, 800));
        cursos.add(new Curso("Cursos profesional de DB", 4.5f, 70, 700));
        cursos.add(new Curso("Cursos profesional de Android", 7.5f, 10, 400));
        cursos.add(new Curso("Cursos profesional de Escritura", 1.5f, 10, 300));
    }

    public static void obtenerCursosDuracionMayorA5Horas() {
        System.out.println("Obtener la cantidad de cursos con una duración mayor a 5 horas\n");
        System.out.println(cursos.stream().filter(curso -> curso.getDuracion() > 5.0f).map(Curso::getTitulo).collect(Collectors.toList()));
        System.out.println(cursos.stream().filter(curso -> curso.getDuracion() > 5.0f).count());
    }

    public static void obtenerCursosDuracionMenorA2Horas() {
        System.out.println("Obtener la cantidad de cursos con una duración menor a 2 horas.\n");
        System.out.println(cursos.stream().filter(curso -> curso.getDuracion() < 2.0f).map(Curso::getTitulo).collect(Collectors.toList()));
        System.out.println(cursos.stream().filter(curso -> curso.getDuracion() < 2.0f).count());
    }

    public static void listarCursosVideosMayorA50() {
        System.out.println("Listar el título de todos aquellos cursos con una cantidad de vídeos mayor a 50.\n");
        cursos.stream().filter(curso -> curso.getVideos() > 50).map(Curso::getTitulo).forEach(System.out::println);
    }

    public static void mostrar3CursosMayorDuracion() {
        System.out.println("Mostrar en consola el título de los 3 cursos con mayor duración.\n");
        cursos.stream().sorted(Comparator.comparing(Curso::getDuracion).reversed()).limit(3).map(Curso::getTitulo).forEach(System.out::println);
    }

    public static void mostrarDuracionTotalCursos() {
        System.out.println("Mostrar en consola la duración total de todos los cursos\n");
        Float duracionTotal = cursos.stream().map(Curso::getDuracion).reduce(Float::sum).orElse(0F);
        System.out.println(duracionTotal);
    }

    public static void mostrarCursosSuperanPromedioDuracion() {
        System.out.println("Mostrar en consola todos aquellos libros que superen el promedio en cuanto a duración se refiere.\n");
        Double duracionPromedio = cursos.stream().mapToDouble(Curso::getDuracion).average().orElse(0);
        cursos.stream().filter(curso -> curso.getDuracion() > duracionPromedio).map(Curso::getTitulo).forEach(System.out::println);
    }

    public static void mostrarCursosMenos500Alumnos() {
        System.out.println("Mostrar en consola la duración de todos aquellos cursos que tengan una cantidad de alumnos inscritos menor a 500.\n");
        cursos.stream().filter(curso -> curso.getAlumnos() < 500).map(Curso::getDuracion).forEach(System.out::println);
    }

    public static void obtenerCursoMayorDuracion() {
        System.out.println("Obtener el curso con mayor duración.\n");
        System.out.println(cursos.stream().max(Comparator.comparingDouble(Curso::getDuracion)).map(Curso::getTitulo).orElse("No hay cursos"));
    }

    public static void crearListaTitulosCursos() {
        System.out.println("Crear una lista de Strings con todos los titulos de los cursos\n");
        List<String> titulos = cursos.stream().map(Curso::getTitulo).collect(Collectors.toList());
        titulos.forEach(System.out::println);
    }

    public static void main(String[] args) {
        CursoManager.obtenerCursosDuracionMayorA5Horas();
        CursoManager.obtenerCursosDuracionMenorA2Horas();
        CursoManager.listarCursosVideosMayorA50();
        CursoManager.mostrar3CursosMayorDuracion();
        CursoManager.mostrarDuracionTotalCursos();
        CursoManager.mostrarCursosSuperanPromedioDuracion();
        CursoManager.mostrarCursosMenos500Alumnos();
        CursoManager.obtenerCursoMayorDuracion();
        CursoManager.crearListaTitulosCursos();
    }

    // Clase anidada Curso
    public static class Curso {
        private String titulo;
        private float duracion;
        private int videos;
        private int alumnos;

        public Curso(String titulo, float duracion, int videos, int alumnos) {
            this.titulo = titulo;
            this.duracion = duracion;
            this.videos = videos;
            this.alumnos = alumnos;
        }

        public String getTitulo() {
            return titulo;
        }

        public float getDuracion() {
            return duracion;
        }

        public int getVideos() {
            return videos;
        }

        public int getAlumnos() {
            return alumnos;
        }
    }
}
