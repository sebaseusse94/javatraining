package streams;

import java.util.*;
import java.util.stream.Collectors;

public class StreamsMedium {
    private static Map<Integer, List<Integer>> usersMap;

    static {
        usersMap = new HashMap<>();
        usersMap.put(1, Arrays.asList(2, 3, 4));
        usersMap.put(2, Arrays.asList(1, 3, 5));
        usersMap.put(3, Arrays.asList(1, 2, 4, 5));
        usersMap.put(4, Arrays.asList(1, 3));
        usersMap.put(5, Arrays.asList(2, 3));
        usersMap.put(6, Arrays.asList(1, 4, 5));
        usersMap.put(7, Arrays.asList(2, 6));
        usersMap.put(8, Arrays.asList(3, 5, 7));
        usersMap.put(9, Arrays.asList(1, 8));
        usersMap.put(10, Arrays.asList(4, 7, 9));
    }

    public static void main(String[] args) {
        getPopularId();

        wordFrequency("I am doing an interview at xYz I work for xyZ bla bla Bla");

        characterFrequency("aaabbbddccllddlsjsjs");
    }

    private static void getPopularId(){
        Map<Integer, Long> groups = usersMap.entrySet().stream().flatMap(entry -> entry.getValue().stream())
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
        System.out.println(groups.entrySet().stream().map(entry->entry.getValue()).sorted().findFirst());
    }

    private static void wordFrequency(String text) {
        String[] split = text.split("\\s+");
        Map<String, Long> groups = Arrays.stream(split)
                .collect(Collectors.groupingBy(word -> word, Collectors.counting()));
        System.out.println("Word Frequency: " + groups);
    }

    private static void characterFrequency(String text) {
        Map<Character, Long> groups = text.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
        System.out.println("Character Frequency: " + groups);
    }


}
