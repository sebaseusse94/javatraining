package problems;

import java.util.*;
import java.util.stream.Collectors;

public class LogicProblems {
    public static void main(String[] args) {
        List<Integer> values = Arrays.asList(1, 1, 2, 2, 4, 4, 5, 5, 5);
        int result = pickingNumbers(values);
        System.out.println("Picking Numbers Result: " + result);

        System.out.println("Anagram Groups: " + groups());

        System.out.println("Is Balanced: " + isBalanced("(())"));

        getMaximunSum(new int[]{1000, 200, 2000});

        maxSumSubarray(new int[]{2, 3, 2, 5, 5, 5, 1, 5, 5}, 3);

        SumSubArrayOfGivenNumber(new int[]{0}, 0);

        coupleNumbersSumTheGivenNumber(new int[]{1, 2, 3, 7, 5}, 6);

        longestSecuenceOrdered(new int[]{1, 2, 3, 5, 6, 7, 8});

        System.out.println("Average Salary for Henry: " + findSalaryAverageByName());

        List<Integer> list = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));

        System.out.println("Binary Search Result: " + binarySearch(list, 1));

        int[] array = {2, 4, 5, 6, 7, 8, 9};

        System.out.println("Min Value of Arrays is: "+minOfArray(array,0,array.length-1));

        System.out.println("Max Value of Arrays is: "+maxOfArray(array,0,array.length-1));
    }

    private static double findSalaryAverageByName() {
        class Employee {
            private String empName;
            private double salary;

            public Employee(String empName, double salary) {
                this.empName = empName;
                this.salary = salary;
            }

            public String getEmpName() {
                return empName;
            }

            public double getSalary() {
                return salary;
            }
        }

        List<Employee> employees = Arrays.asList(
                new Employee("Alice", 5000),
                new Employee("Bob", 7000),
                new Employee("Charlie", 6000),
                new Employee("David", 8000),
                new Employee("Eve", 5500),
                new Employee("Frank", 7500),
                new Employee("Grace", 6200),
                new Employee("Henry", 7100),
                new Employee("Henry", 7100),
                new Employee("Henry", 7200)
        );

        String givenName = "Henry";
        return employees.stream()
                .filter(employee -> employee.getEmpName().equals(givenName))
                .mapToDouble(Employee::getSalary)
                .average()
                .orElse(0.0);
    }

    public static int pickingNumbers(List<Integer> a) {
        Map<Integer, Integer> numbers = new HashMap<>();
        int maxValue = 0;
        int currentValue;

        for (int number : a) {
            numbers.put(number, numbers.getOrDefault(number, 0) + 1);
        }

        for (int key : numbers.keySet()) {
            currentValue = numbers.get(key) + numbers.getOrDefault(key + 1, 0);
            maxValue = Math.max(currentValue, maxValue);
        }

        return maxValue;
    }

    private static List<List<String>> groups() {
        Map<String, List<String>> groups = new HashMap<>();
        String[] strs = {"eat", "tea", "tan", "ate", "nat", "bat"};

        for (String str : strs) {
            char[] chars = str.toCharArray();
            Arrays.sort(chars);
            String sortedKey = new String(chars);

            if (!groups.containsKey(sortedKey)) {
                groups.put(sortedKey, new ArrayList<>());
            }

            groups.get(sortedKey).add(str);
        }

        return new ArrayList<>(groups.values());
    }

    private static boolean isBalanced(String expression) {
        Map<String, String> validCharacters = new HashMap<>();
        validCharacters.put("(", ")");
        validCharacters.put("[", "]");
        validCharacters.put("{", "}");

        Stack<String> stack = new Stack<>();

        for (char c : expression.toCharArray()) {
            String ch = String.valueOf(c);
            if (validCharacters.containsKey(ch)) {
                stack.push(ch);
            } else if (validCharacters.containsValue(ch) && !stack.isEmpty()) {
                stack.pop();
            }
        }

        return stack.isEmpty();
    }

    private static void getMaximunSum(int[] numbers) {
        int maxSum = 0;
        int sum = 0;

        for (int i = 0; i < 2; i++) {
            sum += numbers[i];
            maxSum = sum;
        }

        for (int i = 2; i < numbers.length; i++) {
            sum += numbers[i] - numbers[i - 2];
            if (sum > maxSum) {
                maxSum = sum;
            }
        }

        System.out.println("Maximum Sum of Subarray of Size 2: " + maxSum);
    }

    private static void maxSumSubarray(int[] numbers, int k) {
        int maxSum = 0;
        int sum = 0;
        int pointerLeft = 0;
        int pointerRight = 0;

        for (int i = 0; i < k; i++) {
            sum += numbers[i];
        }

        for (int j = k; j < numbers.length; j++) {
            sum += numbers[j] - numbers[j - k];
            if (sum > maxSum) {
                maxSum = sum;
                pointerRight = j;
                pointerLeft = j - k + 1;
            }
        }

        System.out.println("Maximum Sum Subarray of Size " + k + ": " + maxSum);
        System.out.println("Starting Index: " + pointerLeft);
        System.out.println("Ending Index: " + pointerRight);
    }

    private static void SumSubArrayOfGivenNumber(int[] numbers, int k) {
        int j = 0;
        int sum = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
            while (sum > k && j <= i) {
                sum -= numbers[j];
                j++;
            }

            if (sum == k) {
                System.out.println("Subarray Sum equals " + k + ": Start Index " + j + ", End Index " + i);
            }
        }
    }

    private static void coupleNumbersSumTheGivenNumber(int[] numbers, int k) {
        int j = 0;
        int sum = 0;
        int counter = 0;

        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
            counter++;
            while (sum > k && j <= i) {
                sum -= numbers[j];
                j++;
                counter--;
            }

            if (counter == 2 && sum == k) {
                System.out.println("Couple Sum equals " + k + ": Start Index " + j + ", End Index " + i);
                return;
            }
        }

        System.out.println("No couple found that sums to " + k);
    }

    private static void longestSecuenceOrdered(int[] numbers) {
        int start = 0;
        int maxLength = 0;
        int longestStart = 0;

        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] - numbers[i - 1] == 1) {
                int currentLength = i - start + 1;
                if (currentLength > maxLength) {
                    maxLength = currentLength;
                    longestStart = start;
                }
            } else {
                start = i;
            }
        }

        System.out.println("Longest Ordered Sequence: Start Index " + longestStart + ", Length " + maxLength);
    }

    private static Integer binarySearch(List<Integer> numbers, int value) {
        int mid = (numbers.size()) / 2;

        if (numbers.get(mid) == value) {
            return numbers.get(mid);
        } else if (value < numbers.get(mid)) {
            return binarySearch(numbers.subList(0, mid), value);
        } else {
            return binarySearch(numbers.subList(mid + 1, numbers.size()), value);
        }
    }

    public static int minOfArray(int[] array, int inicio, int fin) {
        if (inicio == fin) {
            return array[inicio];
        }
        int mitad = (inicio + fin) / 2;

        int minLeft = minOfArray(array, inicio, mitad);
        int minRight = minOfArray(array, mitad + 1, fin);

        return minLeft > minRight ? minRight : minLeft;
    }

    public static int maxOfArray(int[] array, int inicio, int fin) {
        if (inicio == fin) {
            return array[inicio];
        }
        int mitad = (inicio + fin) / 2;

        int maxLeft = maxOfArray(array, inicio, mitad);
        int maxRight = maxOfArray(array, mitad + 1, fin);

        return maxLeft < maxRight ? maxRight : maxLeft;
    }
}
