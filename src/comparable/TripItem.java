package comparable;

import java.util.*;
import java.util.stream.Collectors;

public class TripItem implements Comparable<TripItem> {
    String destination;
    Date date;

    public TripItem(String destination, Date date) {
        this.destination = destination;
        this.date = date;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object obj) {
        TripItem item = (TripItem) obj;
        return Objects.equals(this.destination, item.destination);
    }

    @Override
    public int compareTo(TripItem tripItem) {
        return tripItem.date.compareTo(this.date);
    }

    public static void main(String[] args) {
        System.out.println("Hello world!");
        TripItem tripItem2 = new TripItem("city tour 2", new Date(2024, 05, 02));
        TripItem tripItem1 = new TripItem("city tour 1", new Date(2024, 05, 01));
        TripItem tripItem4 = new TripItem("city tour 4", new Date(2024, 05, 04));
        TripItem tripItem3 = new TripItem("city tour 3", new Date(2024, 05, 03));
        TripItem tripItem5 = new TripItem("city tour 3", new Date(2024, 05, 03));

        List<TripItem> tripItems = new ArrayList<>();
        tripItems.add(tripItem1);
        tripItems.add(tripItem4);
        tripItems.add(tripItem3);
        tripItems.add(tripItem2);
        tripItems.add(tripItem5);

        //ordered by Date in overriding equals method
        tripItems.stream().sorted().map(item -> item.getDestination()).forEach(System.out::println);
        Set<String> collect = tripItems.stream().sorted().map(item -> item.getDestination()).collect(Collectors.toSet());
        System.out.println(collect);

    }
}

